#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <ctype.h>
#include <termios.h>
#include <pthread.h>
#include <stdbool.h>
#include <fcntl.h>

#define PORT 8080

int main () {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0}, username[100];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nAlamat tidak diketahui\n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nGagal Terkoneksi \n");
        return -1;
    } else{
        printf("Menghubungkan..\n\n");
    }

    char input[1024], input2[1024], name[1024], password[1024], temp;
    valread = read(sock, buffer, 1024);


    while(true){
        printf("Menu: \n1. Login \n2. Register \nq. Quit\n\n");
        printf("Pilih menu: ");
        scanf("%s", input);

        if(strcmp(input, "1") == 0){
            send(sock, "login", 1024, 0);

            printf("Login\nUsername: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);

            memset(buffer, 0, 1024);

            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "success") == 0){
                printf("\nBerhasil Login!\n\n");

                while(true){
                    printf("Menu: \n1. Add\n2. See\n3. Download\n4. Submit\n q. Logout\n\n");
                    printf("Pilih menu: ");
                    scanf("%s", input2);

                    if(strcmp(input2, "1") == 0){break;}
                    if(strcmp(input2, "2") == 0){break;}
                    if(strcmp(input2, "3") == 0){break;}
                    if(strcmp(input2, "4") == 0){break;}
                    if(strcmp(input2, "q") == 0){return 0;}
                }

            }else if(strcmp(buffer, "error") == 0){
                printf("\nGagal Login!\n\n");
            }
        }

        else if(strcmp(input, "2") == 0){
            send(sock, "register", 1024, 0);

            printf("Register\n");
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);

            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "error") == 0){
                printf("\nuser/pasword sudah ada!\n");
            }else if(strcmp(buffer, "success") == 0){
                printf("\nRegister berhasil!\n");
            }
            
            memset(buffer, 0, 1024);
        }

        else if(strcmp(input, "q") == 0){
            send(sock, "quit", 1024, 0);
            printf("Shutting Down ");
            sleep(1);
            printf(". ");
            sleep(2);
            printf(". ");
            sleep(2);
            printf(". \n");
            return 0;
        }else{
            printf("\nGk usah aneh aneh, udah pusing sisop!!!!");
        }

    memset(buffer, 0, 1024);
    send(sock, "return", 1024, 0);
        
    }
    return 0;
}
