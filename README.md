# soal-shift-sisop-modul-3-I08-2022

## REPORT GROUP I08 
| Nama		 		 | NRP		  | 
| :---         		 |     :---:      | 
| Deon Fitra Tama    | 5025201115     | 
| M. Akmal Riswanda  | 5025201143     | 
| Venia Sollery A H. | 5025201161	  |

## Number 1

### Question

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### Problem A
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

From the problem above we can see that we will need two thread to unzip both file at the same time. In this solution the we make a function that will run two threads. The first thread is to unzip quote.zip and the second is to unzip music.zip.

```c
char *argv1[] =  {"unzip", "quote.zip", "-d", "modul3", NULL};
    char *argv2[] = {"unzip", "music.zip", "-d", "modul3", NULL};
    pthread_t id = pthread_self();

    if(pthread_equal(id,tid[0])){

    child = fork();
        if(child == 0){
            execv("/bin/unzip", argv1);
        }
    }

    else if(pthread_equal(id,tid[1])){
        child = fork();

        if(child== 0){
            execv("/bin/unzip", argv2);
        }
    }
```
## Problem B

Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

This prople ask us to decode the txt file from the folders that we have unzipped. This is done using base64 to decode.

The solution to this problem is it takes two threads to decode both quote and music text. Each of these processes is a process to decode each file. The first thread does the process of decodinf the quote.zip and the second does it for music.zip.

```c
void* decodeText(){

    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/venia/modul3/q%d.txt >> /home/venia/modul3/quote.txt", i);
            base64(cmd);
            enter("/home/venia/modul3/quote.txt");
        }
    }
    else if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/venia/modul3/m%d.txt >> /home/venia/modul3/music.txt", i);
            base64(cmd);
            enter("/home/venia/modul3/music.txt");
        }
    }
}
```
After decoding this problem ask us to seperate each sentence by a newline/enter. This is done by making a function that will seperate the sentences and this function is called in decodeText.

```c
void enter(char* dir){
    int status;
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);
    } 
    else{
        while((wait(&status)) > 0);
    }
}
```
## Problem C

Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

This problem ask us to move the file that has already been decoded to a new folder named hasil.

```c
void moveFile(){
    int status;
    pid_t child_id;
    child_id = fork();

    char *argv[] = {"mkdir", "-p","/home/venia/modul3/hasil", NULL};
    char *argv2[] = {"mv", "/home/venia/modul3/quote.txt", "/home/venia/modul3/hasil", NULL};
    char *argv3[] = {"mv", "/home/venia/modul3/music.txt", "/home/venia/modul3/hasil", NULL};

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        execv("/usr/bin/mkdir", argv);
    }
    else{
        while(wait(&status) > 0);
        pid_t child_id2;
        child_id2 = fork();

        if(child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if(child_id2 == 0){
            execv("/usr/bin/mv", argv2);
        }
        else{
            while(wait(&status) > 0);
            pid_t child_id3;
            child_id3 = fork();

            if(child_id3 < 0){
                exit(EXIT_FAILURE);
            }
            else if(child_id3 == 0){
                execv("/usr/bin/mv", argv3);

            }
        }
    }
}
```
In this solution we will fork to make a new child process. Then we have three argv each one does different task. The first one will make the directory hasil, the second one will move the decoded text to the folder that has just been made, and the last one will do the same for the music text.

## Problem D

Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

In this problem we are asked to zip the file hasil with a password. 

```c
void zipFile(char* pass){
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char *argv[] = {"zip", "-P", pass, "-r", "/home/venia/modul3/hasil.zip", "/home/venia/modul3/hasil", NULL};
        execv("/usr/bin/zip", argv);

    }
    else{
        while((wait(&status)) > 0);
    }

}
```

This is done by making a child process. In that we will use argv to zip the file with the password.


## Number 2
### Question
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### Answer
### Question A
So here we are told to make the client connect to the server. so I used `struct sockaddr_in` to create a socket to connect to the server. then in the server, we create a socket with `new_socket`. To check if a socket can be created and connected we use `inet_pton()` and `connect()`.In the next step to communication, we use `valread` and `buffer` to be sent and received between the server and client or vice versa.
client
```
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0}
```
server
```
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
```

If it is connected to the client, a menu will appear to choose from, namely login, register, and quit. to take a choice from the client we use `strcmp` and if `== 0` then a command will be sent to the server using `send` and the buffer is filled by `login` which will then be answered by the server asking for Username and Password.
```
if(strcmp(input, "1") == 0){
    send(sock, "login", 1024, 0);

    printf("Login\nUsername: ");
    scanf("%c", &temp);
    scanf("%[^\n]", name);
    send(sock, name, strlen(name), 0);

    printf("Password: ");
    scanf("%c", &temp);
    scanf("%[^\n]", password);
    send(sock, password, strlen(password), 0);

    memset(buffer, 0, 1024);
```

The server will check the client's request. If the client asks to log in, the server stores the name and password in the `account` struct. Furthermore, if the user file does not exist, a user file will be created and will be opened by the server.
```
    if(access("users.txt", F_OK) != 0){
        printf("File not found. Creating file.... \n\n");
        file = fopen("users.txt", "a+");
        fclose(file);
    }

    file = fopen("users.txt", "a+");

    int flag = 0;
    char *line = NULL;
    ssize_t len = 0;
    ssize_t file_read;

    while((file_read = getline(&line, &len, file) != -1)) {
        char t_name[1024];
        char t_pass[1024];

        int i = 0;
        while(line[i] != ':'){
            t_name[i] = line[i];
            i++;
        }

        t_name[i] = '\0';
        i++;

        int j = 0;
        while(line[i] != '\n'){
            t_pass[j] = line[i];
            i++;
            j++;
        }

        t_pass[j] = '\0';
        j++;

        if(strcmp(account.name, t_name) == 0 && strcmp(account.password, t_pass) == 0){
            flag = 1;
            send(new_socket, "success", 1024, 0);
            break;
        }
    }
```
Of course, it is almost the same as the Register, which will also send the user and password, but on the server the user and password will be stored in the user.txt file which will later be used for future logins.
```
    if(strcmp(buffer, "register") == 0){

        if(access("users.txt", F_OK) != 0){
            printf("Error..\nmembuat file baru... \n\n");
            file = fopen("users.txt", "a+");
            fclose(file);
        }

        file = fopen("users.txt", "a");

        valread = read(new_socket, account.name, 1024);
        valread = read(new_socket, account.password, 1024);

        int flag = 0;
        char *line = NULL;
        ssize_t len = 0;
        ssize_t file_read;
```
and the data will save using
```
    if(flag == 0 && upper >= 1 && lower >= 1 && number >= 1 && strlen(account.password) >= 6){
        // write file
        fprintf(file, "%s:%s\n", account.name, account.password);
        printf("%s account added successfully\n\n", account.name);

        send(new_socket, "success", 1024, 0);
```
The server will also check whether the password for this register has met the criteria. like 6 letters, there are numbers, there are uppercase and lowercase letters
```
for (int i = 0; i < strlen(account.password); i++){
                if(account.password[i] >= 'A' && account.password[i] <= 'Z') upper++;
                else if(account.password[i] >= 'a' && account.password[i] <= 'z') lower++;
                else if(account.password[i] >= '0' && account.password[i] <= '9') number++;
            }
```

Screenshot
```
```
![img2-1](https://cdn.discordapp.com/attachments/651418861496696833/965235228639653928/unknown.png)
![img2-2](https://cdn.discordapp.com/attachments/651418861496696833/965235511822258196/unknown.png)
## Number 3
### Question

- Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”
- Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
- Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
- Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.
 Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
- Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke serve 

#### Note : 
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system(), **kecuali untuk bagian zip pada soal d**

### Answer 
In the `checkExist()` section from `soal3.c` file, it is used to check whether the file is available or not in the current directory.
```c
int checkExist(const char *NamaFile)
{
    struct stat buffer;
    int exist = stat(NamaFile, &buffer);
    
    if (exist == 0) return 1;
    else return 0;
}
```
The `moveDirectory()` function is used to move files into directories or folders that match the file extension. And if the file is hidden, it will be put in a directory called Hidden.
```c
if (hidden[1] == '.')
    {
        strcpy(dirnamae, "Hidden");
    }
```
Also, if there are files that don't have extensions, they will be put in the Unknown folder.
```c
 else
    {
        strcpy(dirNamaFile, "Unknown");
    }
``` 
Different file extension will move from recursion fuction using `phtread_create` then join the thread using `phtread_join` by its category. 
```c
pthread_t thread;
int err = pthread_create(&thread, NULL, moveDirectory, (void *)path);
pthread_join(thread, NULL);
```
Then, because in the question we are allow to using system function **only in the question d** so for zipping the folder `hartakarun` we can implement system function in the `client.c`. 
```c
system("zip hartakarun.zip -r /home/rish/Sisop/Modul3/hartakarun");
```
To send the result file to the server we should make client socket and the client address so the server after it could catch the data. 

**client address**
```c
memset(&serv_addr, '0', sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);
if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
    perror("\nInvalid address/ Address not supported \n");
    return -1;
}
```

**client socket**
```c
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("\n Socket creation error \n");
    return -1;
}
printf("socket created\n");
```
to make the program connect to the server and working very well we also need to connect the client and the server.

**Connecting client**
```c
int con = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
if(con < 0){
    perror("Connection failed\n");
    return -1;
}
printf("Connected to the server\n");
```

After it, we need to make the server socket and bind the address beetwen this server and the client in `server.c` file.

**Server socket**
```c
if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
    perror("setsockopt");
    exit(EXIT_FAILURE);
}
```
then create server address then bind it,

**Server address and bind Server**
```c
address.sin_family = AF_INET;
address.sin_addr.s_addr = INADDR_ANY;
address.sin_port = htons( PORT );
if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
    perror("bind failed");
    exit(EXIT_FAILURE);
}
printf("Socket successfully binded\n");
```
Then after client and server is connect data from the client will be send into the server. since we need to read each data. We can use `send` as a buffer into the socket.

```c
FILE *fp = fopen(filename, "r");
if(fp == NULL){
    perror("Error in reading file\n");
    return -1;
}

send(sock, filename, strlen(filename), 0);
while((valread = fread(buffer, 1, sizeof(buffer), fp))> 0){
    if(send(sock, buffer, valread, 0) < 0){
        perror("Error in sending file\n");
        return -1;
    }
    bzero(buffer, sizeof(buffer));
}

```
### Result
**categorized file**
![Screenshot_2022-04-17_004220](/uploads/ad1a71aabc2f4cbf986457fcbf3c1b3a/Screenshot_2022-04-17_004220.png)
**send client data to server**
![Screenshot_2022-04-17_004259](/uploads/19e5ae6ec41e1c0a7580c9dce0bc38fe/Screenshot_2022-04-17_004259.png)
### Revision
Question D-E 

