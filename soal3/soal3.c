#include <stdio.h>
#include <sys/types.h>
#include <pthread.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>


//Check existance file in directory
int checkExist(const char *NamaFile)
{
    struct stat buffer;
    int exist = stat(NamaFile, &buffer);
    
    if (exist == 0) return 1;
    else return 0;
}


void *moveDirectory(void *NamaFile)
{
    char cwd[PATH_MAX], dirNamaFile[200], hidden[100], hiddenNamaFile[100], file[100], existfile[100];
    int i;

    strcpy(existfile, NamaFile);
    strcpy(hiddenNamaFile, NamaFile);

    char *nama = strrchr(hiddenNamaFile, '/');
    
    strcpy(hidden, nama);

    //file hidden with starting '.'
    if (hidden[1] == '.')
    {
        strcpy(dirNamaFile, "Hidden");
    }
    
    //File normal
    else if (strstr(NamaFile, ".") != NULL)
    {
        strcpy(file, NamaFile);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        //case not sensitive
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirNamaFile, token);
    }
    //file unknown
    else
    {
        strcpy(dirNamaFile, "Unknown");
    }

    //check existness of a file
    int exist = checkExist(existfile);
    if (exist)
        mkdir(dirNamaFile, 0755);

    //get file name
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {

        char *nama = strrchr(NamaFile, '/');
        char NamanamaFile[200];

        strcpy(NamanamaFile, cwd);
        strcat(NamanamaFile, "/");
        strcat(NamanamaFile, dirNamaFile);
        strcat(NamanamaFile, nama);

        //Move a file
        rename(NamaFile, NamanamaFile);
    }
}

//Recursive list
void listRecursive(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {

            // membuat new path dengan base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);


            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {

                //Create thread to check a move
                pthread_t thread;
                int err = pthread_create(&thread, NULL, moveDirectory, (void *)path);
                pthread_join(thread, NULL);

            }

            listRecursive(path);

        }

    }

    closedir(dir);

}

int main(int argc, char *argv[])
{
  char cwd[PATH_MAX];
  
  if (getcwd(cwd, sizeof(cwd)) != NULL){
      //Usinf recursuve to categorized file from directory
      listRecursive(cwd);
   }

}
