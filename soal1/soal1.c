#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <syslog.h>
#include <pwd.h>
#include <grp.h>
#include <stdint.h>
#include <string.h>

pthread_t tid[4];
pid_t child;



// void unzip(char *fileName){
//     child = fork();
//     char dest[101];
//     char file[101] = "/home/venia/modul3/";
//     strcat(file, fileName);
//     strcat(fileName, ".zip");
//     strcpy(dest, fileName);
//     if(child == 0){
//         char *argv[] = {"unzip", "-o", file, "-d", dest, NULL};
//             execv("/usr/bin/unzip", argv);
//     }
//     else if(child > 0){
//         exit(EXIT_FAILURE);
//     }
// }

// void* unzipp(){
//     pthread_t id = pthread_self();
//     if(pthread_equal(id, tid[0])){
//         unzip("quote");
//     }
//     else if(pthread_equal(id, tid[1])){
//         unzip("music");
//     }
//     return NULL;
// }



void *unzip(void *arg){

    char *argv1[] =  {"unzip", "quote.zip", "-d", "modul3", NULL};
    char *argv2[] = {"unzip", "music.zip", "-d", "modul3", NULL};
    pthread_t id = pthread_self();

    if(pthread_equal(id,tid[0])){

    child = fork();
        if(child == 0){
            execv("/bin/unzip", argv1);
        }
    }

    else if(pthread_equal(id,tid[1])){
        child = fork();

        if(child== 0){
            execv("/bin/unzip", argv2);
        }
    }
        return NULL;
}



void enter(char* dir){
    int status;
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);
    } 
    else{
        while((wait(&status)) > 0);
    }
}

void base64(char* text){

    int status;
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }

    else if(child_id == 0){
        execl("/bin/sh", "sh", "-c", text, (char *)0);

    }
    else{
        while((wait(&status)) > 0);
    }
}

void* decodeText(){

    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/venia/modul3/q%d.txt >> /home/venia/modul3/quote.txt", i);
            base64(cmd);
            enter("/home/venia/modul3/quote.txt");
        }
    }
    else if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/venia/modul3/m%d.txt >> /home/venia/modul3/music.txt", i);
            base64(cmd);
            enter("/home/venia/modul3/music.txt");
        }
    }
}

void moveFile(){
    int status;
    pid_t child_id;
    child_id = fork();

    char *argv[] = {"mkdir", "-p","/home/venia/modul3/hasil", NULL};
    char *argv2[] = {"mv", "/home/venia/modul3/quote.txt", "/home/venia/modul3/hasil", NULL};
    char *argv3[] = {"mv", "/home/venia/modul3/music.txt", "/home/venia/modul3/hasil", NULL};

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        execv("/usr/bin/mkdir", argv);
    }
    else{
        while(wait(&status) > 0);
        pid_t child_id2;
        child_id2 = fork();

        if(child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if(child_id2 == 0){
            execv("/usr/bin/mv", argv2);
        }
        else{
            while(wait(&status) > 0);
            pid_t child_id3;
            child_id3 = fork();

            if(child_id3 < 0){
                exit(EXIT_FAILURE);
            }
            else if(child_id3 == 0){
                execv("/usr/bin/mv", argv3);

            }
        }
    }
}

void zipFile(char* pass){
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char *argv[] = {"zip", "-P", pass, "-r", "/home/venia/modul3/hasil.zip", "/home/venia/modul3/hasil", NULL};
        execv("/usr/bin/zip", argv);

    }
    else{
        while((wait(&status)) > 0);
    }

}

int main(){
    int status;
    pid_t child_id = fork();
    int err;

    char* username = "venia";
    char password[100] = "mihinomenesty";
    strcat(password, username);

    if(child_id < 0) {
        exit(EXIT_FAILURE);

    } else if (child_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &unzip, NULL);
            if(err) {
              printf("\n can't create thread : [%s]",strerror(err));
            }else{
               printf("\n create thread success %d\n", i);
            }

        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    }
    else{
        while(wait(&status) > 0);
        pid_t child_id2 = fork();

        if(child_id2 < 0){
            exit(EXIT_FAILURE);
        }

        else if(child_id2 == 0){
            for(int i=2; i<4; i++){
                err = pthread_create(&(tid[i]), NULL, &decodeText, NULL);
                if(err != 0){
                    printf("\n can't create thread : [%s]",strerror(err));

                }
                else{
                    printf("\n create thread success %d\n", i);
                }
            }
            pthread_join(tid[2],NULL);
            pthread_join(tid[3],NULL);
        }
        else{
            while((wait(&status)) > 0);
            moveFile();
            
            while((wait(&status)) > 0);
            zipFile(pass);

        }

    }

}















